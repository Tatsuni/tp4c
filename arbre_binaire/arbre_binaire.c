/*

    Copyright (c) 2019 Xavier Blanchette-Noël

    L'autorisation est accordée, gracieusement, à toute personne acquérant une
    copie de cette bibliothèque et des fichiers de documentation associés
    (la "Bibliothèque"), de commercialiser la Bibliothèque sans restriction,
    notamment les droits d'utiliser, de copier, de modifier, de fusionner, de
    publier, de distribuer, de sous-licencier et / ou de vendre des copies de
    la Bibliothèque, ainsi que d'autoriser les personnes auxquelles la
    Bibliothèque est fournie à le faire, sous réserve des conditions suivantes:

    La déclaration de copyright ci-dessus et la présente autorisation doivent
    être incluses dans toutes copies ou parties substantielles de la
    Bibliothèque.

    LA BIBLIOTHÈQUE EST FOURNIE "TELLE QUELLE", SANS GARANTIE D'AUCUNE SORTE,
    EXPLICITE OU IMPLICITE, NOTAMMENT SANS GARANTIE DE QUALITÉ MARCHANDE,
    D’ADÉQUATION À UN USAGE PARTICULIER ET D'ABSENCE DE CONTREFAÇON. EN AUCUN
    CAS, LES AUTEURS OU TITULAIRES DU DROIT D'AUTEUR NE SERONT RESPONSABLES DE
    TOUT DOMMAGE, RÉCLAMATION OU AUTRE RESPONSABILITÉ, QUE CE SOIT DANS LE
    CADRE D'UN CONTRAT, D'UN DÉLIT OU AUTRE, EN PROVENANCE DE, CONSÉCUTIF À OU
    EN RELATION AVEC LA BIBLIOTHÈQUE OU SON UTILISATION, OU AVEC D'AUTRES
    ÉLÉMENTS DE LA BIBLIOTHÈQUE.

*/

/*
    \file arbre_binaire.c
    Fichier contenant les routines pour les arbres binaires.
*/ 

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "arbre_binaire.h"

#define TAILLE_TAMPON_FICHIER 4096

int decoder_tampon_arbre_binaire(noeudArbre* arbre, int* tampon, int index);
int formater_tampon_arbre_binaire(noeudArbre* arbre, int* tampon, int index);

struct noeudArbre {
    int valeur;
    noeudArbre* enfantGauche;
    noeudArbre* enfantDroite;
    bool aErreur;
    char* messageErreur;
};

/*
    \brief Fonction qui créer un arbre binaire en insérant la valeur "valeur" dans la racine de l'arbre.
    \param int valeur La valeur à mettre dans la racine de l'arbre.
    \return Retourne le pointeur de la racine de l'arbre.
*/
noeudArbre* creer_arbre_binaire(int valeur) 
{
    noeudArbre* noeud = calloc(1, sizeof(struct noeudArbre));

    if (noeud) {
        noeud->valeur = valeur;
        noeud->enfantGauche = NULL;
        noeud->enfantDroite = NULL;
        noeud->aErreur = false;
        noeud->messageErreur = calloc(ERREUR_TAILLE, sizeof(char));
    }

    return noeud;
}

/*
    \brief Fonction qui détruit un arbre binaire et qui libère l'espace mémoire de cet arbre.
    \param noeudArbre* arbre L'arbre à détruire.
*/
void detruire_arbre_binaire(noeudArbre* arbre)
{
    if (arbre->enfantGauche != NULL) {
        detruire_arbre_binaire(arbre->enfantGauche);
    }   
    if (arbre->enfantDroite != NULL) {
        detruire_arbre_binaire(arbre->enfantDroite);
    }

    free(arbre->messageErreur);
    free(arbre);
}

/* 
    \brief Fonction qui retourne le 
     de noeuds que l'arbre "arbre" contient.
    \param noeudArbre* arbre L'arbre à parcourir.
    \return Retourne le nombre d'élément dans l'arbre.
*/
int nombre_elements_arbre_binaire(noeudArbre* arbre)
{
    int nombreEnfantGauche = 0;
    int nombreEnfantDroite = 0;

    if (arbre->enfantGauche != NULL) {
        nombreEnfantGauche = nombre_elements_arbre_binaire(arbre->enfantGauche);
    } 
    if (arbre->enfantDroite != NULL) {
        nombreEnfantDroite = nombre_elements_arbre_binaire(arbre->enfantDroite);
    }

    return nombreEnfantDroite + nombreEnfantGauche + 1;
}

/*
    \brief Fonction qui retourne le nombre de feuilles contenues dans l'arbre "arbre".
    \param noeudArbre* arbre L'arbre à parcourir.
    \return Le nombre de feuilles dans l'arbre.
*/
int nombre_feuilles_arbre_binaire(noeudArbre* arbre)
{
    int nombreFeuilleGauche = 0;
    int nombreFeuilleDroite = 0;
    int nombreFeuille = 0;

    if (arbre->enfantGauche != NULL) {
        nombreFeuilleGauche = nombre_feuilles_arbre_binaire(arbre->enfantGauche);
    }
    if (arbre->enfantDroite != NULL) {
        nombreFeuilleDroite = nombre_feuilles_arbre_binaire(arbre->enfantDroite);
    }
    if (arbre->enfantGauche == NULL && arbre->enfantDroite == NULL) {
        nombreFeuille++;
    }

    return nombreFeuilleDroite + nombreFeuilleGauche + nombreFeuille;
}

/*
    \brief Fonction qui retourne la hauteur de l'arbre "arbre".
    \param arbre_binaire* arbre L'arbre à parcourir.
    \return La hauteur de l'arbre.
*/
int hauteur_arbre_binaire(noeudArbre* arbre)
{
    int profondeurGauche = -1;
    int profondeurDroite = -1;
    int plusGrandeProfondeur = -1;

    if (arbre->enfantGauche != NULL) {
        profondeurGauche = hauteur_arbre_binaire(arbre->enfantGauche);
    }
    if (arbre->enfantDroite != NULL) {
        profondeurDroite = hauteur_arbre_binaire(arbre->enfantDroite);
    }
    if (profondeurGauche > profondeurDroite) {
        plusGrandeProfondeur = profondeurGauche;
    } else {
        plusGrandeProfondeur = profondeurDroite;
    }

    return plusGrandeProfondeur + 1;

}

/*
    \brief Fonction qui retourne l'élément à la racine de l'arbre.
    \param noeudArbre* arbre L'arbre à parcourir.
    \return L'élément à la racine de l'arbre.
*/
int element_arbre_binaire(noeudArbre* arbre)
{
    return arbre->valeur;
}

/*
    \brief Fonction qui modifie l'élément à la racine de l'arbre "arbre" par la valeur "valeur".
    \param noeudArbre* arbre L'arbre à modifier la valeur de la racine.
    \param int valeur La valeur qui replacera celle de la racine de l'arbre.
*/
void modifier_element_arbre_binaire(noeudArbre* arbre, int valeur)
{
    arbre->valeur = valeur;
}

/*
    \brief Fonction qui parcourt l'arbre à la recherche de la valeur "valeur". Retourne vrai si
    la valeur est contenue dans l'arbre, sinon retourne faux.
    \param arbre_binaire* arbre L'arbre à parcourir.
    \param int valeur La valeur à verifier si elle est contenue dans l'arbre.
    \return Retourne vrai si la valeur est contenue dans l'arbre, sinon retourne faux.
*/
bool contient_element_arbre_binaire(noeudArbre* arbre, int valeur)
{
    bool elementPresent = false;

    if (arbre->valeur == valeur) {
        elementPresent = true;
    } else {
        if (arbre->enfantGauche != NULL && !elementPresent) { 
            elementPresent = contient_element_arbre_binaire(
                                                arbre->enfantGauche, valeur);
        }
        if (arbre->enfantDroite != NULL && elementPresent == false) {
            elementPresent = contient_element_arbre_binaire(
                                                arbre->enfantDroite, valeur);
        }
    }

    return elementPresent;
}

/*
    \brief Fonction qui retourne le sous-arbre enfant gauche de l'arbre "arbre" ou NULL si
    l'arbre n'a pas d'enfant gauche.
    \param arbre_binaire* arbre L'arbre à verifier.
    \return Retourne le sous-arbre gauche de l'arbre "arbre".
*/
noeudArbre* enfant_gauche_arbre_binaire(noeudArbre* arbre)
{
    return arbre->enfantGauche;
}

/*
    \brief Fontion qui créer un enfant gauche sur l'arbre "arbre". Si l'arbre a déjà un enfant gauche,
    la fonction ne modifie rien et retourne une erreur.
    \param arbre_binaire* arbre L'arbre où rajouter le nouvel enfant.
    \param int valeur La valeur à insérer dans le nouvel enfant.
*/
void creer_enfant_gauche_arbre_binaire(noeudArbre* arbre, int valeur)
{
    retirer_erreur_arbre_binaire(arbre);

    if (arbre->enfantGauche == NULL) {
        arbre->enfantGauche = creer_arbre_binaire(valeur);
    } else {
        inscrire_erreur_arbre_binaire(arbre, 
                                    "Ce noeud contient déjà un enfant gauche.");
    }
}

/*
    \brief Fonction qui retire l'enfant gauche de l'arbre "arbre" ainsi que tous les sous-arbres de
    l'enfant originel.
    \param noeudArbre* arbre L'arbre où retirer son enfant gauche.
*/
void retirer_enfant_gauche_arbre_binaire(noeudArbre* arbre)
{
    retirer_erreur_arbre_binaire(arbre);

    if (arbre->enfantGauche != NULL) {
        detruire_arbre_binaire(arbre->enfantGauche);
        arbre->enfantGauche = NULL;
    } else {
        inscrire_erreur_arbre_binaire(arbre, 
                                    "L'arbre donné n'a pas d'enfant gauche.");
    }
}

/*
    \brief Fonction qui retourne le sous-arbre enfant droit de l'arbre "arbre" ou NULL si
    l'arbre n'a pas d'enfant droit.
    \param noeudArbre* arbre L'arbre à verifier.
    \return Retourne le sous-arbre droit de l'arbre "arbre".
*/
noeudArbre* enfant_droit_arbre_binaire(noeudArbre* arbre)
{
    return arbre->enfantDroite;
}

/*
    \brief Fontion qui créer un enfant droit sur l'arbre "arbre". Si l'arbre a déjà un enfant droit,
    la fonction ne modifie rien et retourne une erreur.
    \param noeudArbre* arbre L'arbre où rajouter le nouvel enfant.
    \param int valeur La valeur à insérer dans le nouvel enfant.
*/
void creer_enfant_droit_arbre_binaire(noeudArbre* arbre, int valeur)
{
    retirer_erreur_arbre_binaire(arbre);

    if (arbre->enfantDroite == NULL) {
        arbre->enfantDroite = creer_arbre_binaire(valeur);
    } else {
        inscrire_erreur_arbre_binaire(arbre, 
                                    "Ce noeud contient déjà un enfant droit.");
    }
}

/*
    \brief Fonction qui retire l'enfant droit de l'arbre "arbre" ainsi que tous les sous-arbres de
    l'enfant à retirer.
    \param noeudArbre* arbre L'arbre où retirer son enfant droit.
*/
void retirer_enfant_droit_arbre_binaire(noeudArbre* arbre)
{
    retirer_erreur_arbre_binaire(arbre);

    if (arbre->enfantDroite != NULL) {
        detruire_arbre_binaire(arbre->enfantDroite);
        arbre->enfantDroite = NULL;
    } else {
        inscrire_erreur_arbre_binaire(arbre, 
                                "L'arbre donné n'a pas d'enfant droit.\n\r");
    }
}

/*
    \brief Fonction qui formate le tampon selon le nombre d'enfants de chaque arbre.
    \param noeudArbre* arbre L'arbre où la valeur et ses enfants sont placés.
    \param int tampon Le tampon où les données seront sauvegardées.
    \param int index L'index de position que la fonction utilisera pour savoir sa position
    dans le tampon.
    \note Si cette fonction est appelée pour la première fois, l'argument index devrait
    être initialisé à -1.
    \return La fonction retourne l'index i. Cet index n'est utilisé que par la fonction elle-
    même et donc il n'est pas nécessaire de l'assigner à une variable lors du premier appel.
*/
int formater_tampon_arbre_binaire(noeudArbre* arbre, int* tampon, int index)
{
    int i = index;
    i++;
    tampon[i] = arbre->valeur;
    if (arbre->enfantGauche) {
        tampon[i+1] = 1;
    } else {
        tampon[i+1] = 0;
    }
    i++;
    if (arbre->enfantDroite) {
        tampon[i+1] = 1;
    } else {
        tampon[i+1] = 0;
    }
    i++;
    if (arbre->enfantGauche) {
        i = formater_tampon_arbre_binaire(arbre->enfantGauche, tampon, i);
    }
    if (arbre->enfantDroite) {
        i = formater_tampon_arbre_binaire(arbre->enfantDroite, tampon, i);
    }

    return i;
}

/*
    \brief Fonction qui décode le tampon d'un fichier pour reconstruire un arbre
    avec sa structure complète.
    \param noeudArbre* arbre L'arbre où la structure sera construite.
    \param int* tampon Le tampon à déconstruire.
    \param int index L'index de position que la fonction utilise pour savoir
    dans quelle section du tampon elle doit lire.
    \note Si cette fonction est appelée pour la première fois, index devrait être
    initialisé à 0.
    \return La fonction retourne l'index i. Cet index n'est utilisé que par la fonction elle-
    même et donc il n'est pas nécessaire de l'assigner à une variable lors du premier appel.
*/
int decoder_tampon_arbre_binaire(noeudArbre* arbre, int* tampon, int index)
{
    int i = index;
    arbre->valeur = tampon[i];
    if (tampon[index + 1] == 1) {
        creer_enfant_gauche_arbre_binaire(arbre, tampon[i + 3]);
        i = i + 3;
    }
    if (arbre->enfantGauche) {
        i = decoder_tampon_arbre_binaire(arbre, tampon, i);
    }
    if (tampon[index + 2] == 1) {
        creer_enfant_droit_arbre_binaire(arbre, tampon[i + 3]);
        i = i + 3;
    }
    if (arbre->enfantDroite) {
        i = decoder_tampon_arbre_binaire(arbre, tampon, i);
    }

    return i;
}


/* 
    \brief Fonction qui sauvegarde le contenu d'un arbre binaire dans un fichier binaire "nom_fichier".
    \param noeudArbre* arbre L'arbre qui sera sauvegarder dans le fichier.
    \param char* nom_fichier Le nom du fichier où stocker l'information.
*/
void sauvegarder_arbre_binaire(noeudArbre* arbre, char* nom_fichier)
{
    retirer_erreur_arbre_binaire(arbre);
    FILE* fichier;
    int* tampon;
    int grosseurTampon = nombre_elements_arbre_binaire(arbre) * 3;
    fichier = fopen(nom_fichier, "wb");

    if (fichier) {
        tampon = calloc(grosseurTampon, sizeof(int));
        if (tampon) {
            formater_tampon_arbre_binaire(arbre, tampon, -1);
            fwrite(tampon, sizeof(int), grosseurTampon, fichier);
            for (int i = 0; i < nombre_elements_arbre_binaire(arbre) * 3; i++) {
                printf("%d\n", tampon[i]);
            }
            fclose(fichier);
        } else {
            inscrire_erreur_arbre_binaire(arbre, 
                                "Mémoire insuffisante pour créer le tampon.");
        }
    } else {
        inscrire_erreur_arbre_binaire(arbre, 
                    "Erreur lors de l'ouverture du fichier.");
    }
}

/*
    \brief Fonction qui prend le contenu d'un fichier binaire et qui le charge dans un nouvel arbre.
    \param char* nom_fichier Le nom du fichier où les informations sont storées.
    \note charger_arbre_binaire fera appel à creer_arbre_binaire.
    \return Retourne le pointeur de la racine du nouvel arbre chargé.
*/
noeudArbre* charger_arbre_binaire(char *nom_fichier)
{
    noeudArbre* arbre;
    FILE* fichier;
    int* tampon;
    int grosseurTampon = 0;
    int grosseurFichier = 0;
    fichier = fopen(nom_fichier, "rb");
    if (fichier) {
        fseek(fichier, 0, SEEK_END);
        grosseurFichier = ftell(fichier);
        if (grosseurFichier % sizeof(int) == 0) {
            grosseurTampon = grosseurFichier / sizeof(int);
            tampon = calloc(grosseurTampon, sizeof(int));
            if (tampon) {
                fseek(fichier, 0, SEEK_SET);
                fread(tampon, grosseurTampon, sizeof(int), fichier);
                arbre = creer_arbre_binaire(tampon[0]);
                if (arbre) {
                    decoder_tampon_arbre_binaire(arbre, tampon, 0);
                }
                for (int i = 0; i < 5; i++) {
                    printf("%d\n", tampon[i]);
                }
            } else {
                arbre = creer_arbre_binaire(0);
                inscrire_erreur_arbre_binaire(arbre, "Mémoire insuffisante.");
            }
        } else {
            arbre = creer_arbre_binaire(0);
            inscrire_erreur_arbre_binaire(arbre, "Fichier corrompu.");
        }
    } else {
        arbre = creer_arbre_binaire(0);
        inscrire_erreur_arbre_binaire(arbre, 
                                    "Impossible d'ouvrir le fichier donné.");
    }

    return arbre;
}

/*
    \brief Fonction qui retourne vrai si une erreur est survenue lors du dernier appel de fonction
    de la librairie, sinon retourne faux.
    \param noeudArbre* arbre L'arbre où le boolean est gardé.
    \return Retourne vrai/faux si une erreur est survenue.
*/
bool a_erreur_arbre_binaire(noeudArbre* arbre)
{
    return arbre->aErreur;
}

/*
    \brief Fonction qui retourne un message d'erreur si "a_erreur_arbre_binaire" est vrai, 
    sinon retourne rien.
    \param noeudArbre* arbre L'arbre où le message d'erreur est gardé.
    \return Retourne le message d'erreur.
*/
char* erreur_arbre_binaire(noeudArbre* arbre)
{
    if (!arbre->aErreur) {
        strncpy(arbre->messageErreur, "", ERREUR_TAILLE);
    }

    return arbre->messageErreur;
}

/*
    \brief Fonction qui insert un message d'erreur dans l'arbre binaire. Met également 
    "a_erreur_arbre_binaire" à vrai.
    \param noeudArbre* arbre L'arbre où le message sera inscrit.
    \param char* erreur Le message d'erreur à mettre dans l'arbre.
*/
void inscrire_erreur_arbre_binaire(noeudArbre* arbre, const char* erreur)
{
    arbre->aErreur = true;
    strncpy(arbre->messageErreur, erreur, ERREUR_TAILLE);
}

/*
    \brief Fonction qui met "a_erreur_arbre_binaire" à faux.
    \param noeudArbre* arbre L'arbre où "a_erreur_arbre_binaire" sera changé à faux.
*/
void retirer_erreur_arbre_binaire(noeudArbre* arbre)
{
    arbre->aErreur = false;
}