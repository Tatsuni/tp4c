#include <Python.h>
#include "arbre_binaire.h"

PyObject* gestionnaireErreur;

PyObject* creer(PyObject* a_self, PyObject* a_args)
{
	noeudArbre* arbre = NULL;
	int valeur;
	PyObject* resultat = NULL;
	if (PyArg_ParseTuple(a_args, "i", &valeur)) {
		arbre = creer_arbre_binaire(valeur);
		if (arbre) {
			if (!a_erreur_arbre_binaire(arbre)) {
				resultat = Py_BuildValue("i", arbre);
			} else {
				PyErr_SetString(gestionnaireErreur, erreur_arbre_binaire(arbre));
				detruire_arbre_binaire(arbre);
			}
		}
	}

	return resultat;
}

static PyObject* detruire(PyObject* a_self, PyObject* a_args)
{
	noeudArbre* arbre = NULL;
	PyObject* resultat = NULL;
	if (PyArg_ParseTuple(a_args, "n", &arbre)) {
			detruire_arbre_binaire(arbre);
			Py_INCREF(Py_None);
			resultat = Py_None;
	}

	return resultat;
}

static PyObject* nombre_elements(PyObject* a_self, PyObject* a_args) 
{
	noeudArbre* arbre = NULL;
	int nombreElements;
	PyObject* resultat = NULL;
	if (PyArg_ParseTuple(a_args, "n", &arbre)) {
		nombreElements = nombre_elements_arbre_binaire(arbre);
		if (!a_erreur_arbre_binaire(arbre)) {
			resultat = Py_BuildValue("i", nombreElements);
		} else {
			PyErr_SetString(gestionnaireErreur, erreur_arbre_binaire(arbre));
		}
	}

	return resultat;
}

static PyObject* nombre_feuilles(PyObject* a_self, PyObject* a_args)
{
	noeudArbre* arbre = NULL;
	int nombreFeuilles;
	PyObject* resultat = NULL;
	if (PyArg_ParseTuple(a_args, "n", &arbre)) {
		nombreFeuilles = nombre_feuilles_arbre_binaire(arbre);
		if (!a_erreur_arbre_binaire(arbre)) {
			resultat = Py_BuildValue("i", nombreFeuilles);
		} else {
			PyErr_SetString(gestionnaireErreur, erreur_arbre_binaire(arbre));
		}
	}

	return resultat;
}

static PyObject* hauteur(PyObject* a_self, PyObject* a_args) 
{
	noeudArbre* arbre = NULL;
	int hauteurArbre;
	PyObject* resultat = NULL;
	if (PyArg_ParseTuple(a_args, "n", &arbre)) {
		hauteurArbre = hauteur_arbre_binaire(arbre);
		if (!a_erreur_arbre_binaire(arbre)) {
			resultat = Py_BuildValue("i", hauteurArbre);
		} else {
			PyErr_SetString(gestionnaireErreur, erreur_arbre_binaire(arbre));
		}
	}

	return resultat;
}

static PyObject* element(PyObject* a_self, PyObject* a_args) 
{
	noeudArbre* arbre = NULL;
	int elementArbre;
	PyObject* resultat = NULL;
	if (PyArg_ParseTuple(a_args, "n", &arbre)) {
		elementArbre = element_arbre_binaire(arbre);
		if (!a_erreur_arbre_binaire(arbre)) {
			resultat = Py_BuildValue("i", elementArbre);
		} else {
			PyErr_SetString(gestionnaireErreur, erreur_arbre_binaire(arbre));
		}
	}

	return resultat;
}

static PyObject* modifier_element(PyObject* a_self, PyObject* a_args) 
{
	noeudArbre* arbre = NULL;
	int nouvelElement;
	PyObject* resultat = NULL;
	if (PyArg_ParseTuple(a_args, "ni", &arbre, &nouvelElement)) {
		modifier_element_arbre_binaire(arbre, nouvelElement);
		if (!a_erreur_arbre_binaire(arbre)) {
			Py_INCREF(Py_None);
			resultat = Py_None;
		} else {
			PyErr_SetString(gestionnaireErreur, erreur_arbre_binaire(arbre));
		}
	}

	return resultat;
}
/*
	\note La variable pyBuildValueBool est utilisée pour Py_BuildValue en passant "p" comme argument.
	Le nom représente pas bien la variable mais je ne trouvais pas d'autre noms.
*/
static PyObject* contient_element(PyObject* a_self, PyObject* a_args) 
{
	noeudArbre* arbre = NULL;
	bool elementContenu = false;
	int pyBuildValueBool = 0;
	int elementAVerifier;
	PyObject* resultat = NULL;
	if (PyArg_ParseTuple(a_args, "ni", &arbre, &elementAVerifier)) {
		elementContenu = contient_element_arbre_binaire(arbre, elementAVerifier);
		if (!a_erreur_arbre_binaire(arbre)) {
			if (elementContenu) {
				pyBuildValueBool = 1;
				resultat = Py_BuildValue("p", pyBuildValueBool);
			} else {
				resultat = Py_BuildValue("p", pyBuildValueBool);
			}
		} else {
			PyErr_SetString(gestionnaireErreur, erreur_arbre_binaire(arbre));
		}
	}

	return resultat;
}

static PyObject* enfant_gauche(PyObject* a_self, PyObject* a_args)
{
	noeudArbre* arbre = NULL;
	noeudArbre* enfantGauche;
	PyObject* resultat = NULL;
	if (PyArg_ParseTuple(a_args, "n", &arbre)) {
		enfantGauche = enfant_gauche_arbre_binaire(arbre);
		if (!a_erreur_arbre_binaire(arbre)) {
			resultat = Py_BuildValue("n", enfantGauche);
		} else {
			PyErr_SetString(gestionnaireErreur, erreur_arbre_binaire(arbre));			
		}
	}

	return resultat;
}

static PyObject* creer_enfant_gauche(PyObject* a_self, PyObject* a_args)
{
	noeudArbre* arbre = NULL;
	int valeur;
	PyObject* resultat = NULL;
	if (PyArg_ParseTuple(a_args, "ni", &arbre, &valeur)) {
		creer_enfant_gauche_arbre_binaire(arbre, valeur);
		if (!a_erreur_arbre_binaire(arbre)) {
			Py_INCREF(Py_None);
			resultat = Py_None;
		} else {
			PyErr_SetString(gestionnaireErreur, erreur_arbre_binaire(arbre));			
		}
	}

	return resultat;
}

static PyObject* retirer_enfant_gauche(PyObject* a_self, PyObject* a_args)
{
	noeudArbre* arbre = NULL;
	PyObject* resultat = NULL;
	if (PyArg_ParseTuple(a_args, "n", &arbre)) {
		retirer_enfant_gauche_arbre_binaire(arbre);
		if (!a_erreur_arbre_binaire(arbre)) {
			Py_INCREF(Py_None);
			resultat = Py_None;
		} else {
			PyErr_SetString(gestionnaireErreur, erreur_arbre_binaire(arbre));			
		}
	}

	return resultat;	
}

static PyObject* enfant_droit(PyObject* a_self, PyObject* a_args)
{
	noeudArbre* arbre = NULL;
	noeudArbre* enfantDroit;
	PyObject* resultat = NULL;
	if (PyArg_ParseTuple(a_args, "n", &arbre)) {
		enfantDroit = enfant_droit_arbre_binaire(arbre);
		if (!a_erreur_arbre_binaire(arbre)) {
			resultat = Py_BuildValue("n", enfantDroit);
		} else {
			PyErr_SetString(gestionnaireErreur, erreur_arbre_binaire(arbre));			
		}
	}

	return resultat;
}

static PyObject* creer_enfant_droit(PyObject* a_self, PyObject* a_args)
{
	noeudArbre* arbre = NULL;
	int valeur;
	PyObject* resultat = NULL;
	if (PyArg_ParseTuple(a_args, "ni", &arbre, &valeur)) {
		creer_enfant_droit_arbre_binaire(arbre, valeur);
		if (!a_erreur_arbre_binaire(arbre)) {
			Py_INCREF(Py_None);
			resultat = Py_None;
		} else {
			PyErr_SetString(gestionnaireErreur, erreur_arbre_binaire(arbre));			
		}
	}

	return resultat;
}

static PyObject* retirer_enfant_droit(PyObject* a_self, PyObject* a_args)
{
	noeudArbre* arbre = NULL;
	PyObject* resultat = NULL;
	if (PyArg_ParseTuple(a_args, "n", &arbre)) {
		retirer_enfant_droit_arbre_binaire(arbre);
		if (!a_erreur_arbre_binaire(arbre)) {
			Py_INCREF(Py_None);
			resultat = Py_None;
		} else {
			PyErr_SetString(gestionnaireErreur, erreur_arbre_binaire(arbre));			
		}
	}

	return resultat;
}

PyMethodDef arbre_binaire_methodes[] = {
	{"creer", creer, METH_VARARGS,
	 "Création de la structure d'arbres binaires."},
	{"detruire", detruire, METH_VARARGS,
	 "Méthode pour détruire un arbre."},
	{"nombre_elements", nombre_elements, METH_VARARGS,
	 "Retourne le nombre d'éléments dans l'arbre."},
	{"nombre_feuilles", nombre_feuilles, METH_VARARGS,
	 "Retourne le nombre de feuilles dans l'arbre."},
	{"hauteur", hauteur, METH_VARARGS,
	 "Retourne la hauteur de l'arbre."},
	{"element", element, METH_VARARGS,
	 "Retourne l'élément à la racine de l'arbre."},
	{"modifier_element", modifier_element, METH_VARARGS,
	 "Modifie l'élément par la valeur 'valeur' dans la racine de l'arbre."},
	{"contient_element", contient_element, METH_VARARGS,
	 "Retourne vrai si l'élément 'valeur' est contenu dans l'arbre."},
	{"enfant_gauche", enfant_gauche, METH_VARARGS,
	 "Retourne l'enfant gauche de l'arbre donné."},
	{"creer_enfant_gauche", creer_enfant_gauche, METH_VARARGS,
	 "Créer un enfant gauche à la racine de l'arbre donné."},
	{"retirer_enfant_gauche", retirer_enfant_gauche, METH_VARARGS,
	 "Libère l'enfant gauche de l'arbre donné."},
	{"enfant_droit", enfant_droit, METH_VARARGS,
	 "Retourne l'enfant droit de l'arbre donné."},
	{"creer_enfant_droit", creer_enfant_droit, METH_VARARGS,
	 "Créer un enfant droit à la racine de l'arbre donné."},
	{"retirer_enfant_droit", retirer_enfant_droit, METH_VARARGS,
	 "Libère l'enfant droit de l'arbre donné."},
	{NULL, NULL, 0, NULL}
};

struct PyModuleDef arbre_binaire_module = {
	PyModuleDef_HEAD_INIT,
	"arbre_binaire",
	"Gestionnaire de d'arbres binaires.",
	-1,
	arbre_binaire_methodes
};


PyMODINIT_FUNC PyInit_arbre_binaire(void){
	PyObject* module;
	module = PyModule_Create(&arbre_binaire_module);
	if (module) {
		gestionnaireErreur = PyErr_NewException("arbre_binaire.erreur",
																 NULL, NULL);
		Py_INCREF(gestionnaireErreur);
		PyModule_AddObject(module, "erreur", gestionnaireErreur);
	}
	return module;
}
