import division_externe
try:
    division = division_externe.creer(4,0)
except division_externe.erreur as erreur:
    print("Correct: Erreur reporté pour creer diviseur à 0: " +
            str(erreur))

division = division_externe.creer(4,2)
print("La division est : " + 
        str(division_externe.dividende(division)) +  "/" + 
        str(division_externe.diviseur(division)) + "=" + 
        str(division_externe.division(division))
        )
division_externe.assigne_dividende(division, 5);
division_externe.assigne_diviseur(division, 3);
print("La division est : " + 
        str(division_externe.dividende(division)) +  "/" + 
        str(division_externe.diviseur(division)) + "=" + 
        str(division_externe.division(division))
        )
try:
    division_externe.assigne_diviseur(division, 0);
except division_externe.erreur as erreur:
    print("Correct: Erreur reporté pour assigner diviseur à 0: " +
            str(erreur))
division_externe.detruire(division)
