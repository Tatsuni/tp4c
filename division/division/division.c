#include <stdlib.h>
#include <string.h>
#include "division.h"

struct division_struct {
	int dividende;
	int diviseur;
	bool a_erreur;
	char* erreur;
};

division* creer_division(int a_dividende, int a_diviseur)
{
	division* l_division = calloc(1, sizeof(division));
	if (l_division) {
		l_division->dividende = a_dividende;
		l_division->diviseur = 1;
		l_division->a_erreur = false;
		l_division->erreur = calloc(ERREUR_TAILLE, sizeof(char));
		assigne_diviseur_division(l_division, a_diviseur);
	}
	return l_division;
}

void detruire_division(division* a_division)
{
	free(a_division);
}

int dividende_division(division* a_division)
{
	retirer_erreur_division(a_division);
	return a_division->dividende;
}

void assigne_dividende_division(division* a_division, int a_dividende)
{
	retirer_erreur_division(a_division);
	a_division->dividende = a_dividende;
}

int diviseur_division(division* a_division)
{
	retirer_erreur_division(a_division);
	return a_division->diviseur;
}

void assigne_diviseur_division(division* a_division, int a_diviseur)
{
	retirer_erreur_division(a_division);
	if (a_diviseur == 0) {
		inscrire_erreur_division(a_division,
				"Le diviseur ne peut pas être 0."
			);
	} else {
		a_division->diviseur = a_diviseur;
	}
}

float division_division(division* a_division)
{
	retirer_erreur_division(a_division);
	return a_division->dividende / (float)a_division->diviseur;
}

bool a_erreur_division(division* a_division)
{
	return a_division->a_erreur;
}

char* erreur_division(division* a_division)
{
	return a_division->erreur;
}

void inscrire_erreur_division(division* a_division, const char* a_erreur)
{
	a_division->a_erreur = true;
	strncpy(a_division->erreur, a_erreur, ERREUR_TAILLE);
}

void retirer_erreur_division(division* a_division)
{
	a_division->a_erreur = false;
	strncpy(a_division->erreur, "", ERREUR_TAILLE);
}

