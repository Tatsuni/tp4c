import arbre_binaire

try:
    arbre = arbre_binaire.creer(4)
except arbre_binaire.erreur as erreur:
    print("Correct: Erreur reporté pour creer l'arbre : " + str(erreur))
arbre_binaire.detruire(arbre)