#include <Python.h>
#include "division.h"

PyObject* division_erreur;

PyObject* py_division_creer(PyObject* a_self, PyObject* a_args)
{
	division* l_division = NULL;
	int l_dividende, l_diviseur;
	PyObject* l_result = NULL;
	if(PyArg_ParseTuple(a_args, "ii", &l_dividende, &l_diviseur)) {
		l_division = creer_division(l_dividende, l_diviseur);
		if (l_division) {
			if (!a_erreur_division(l_division)) {
				l_result = Py_BuildValue("n", l_division);
			} else {
				PyErr_SetString(division_erreur, erreur_division(l_division));
				detruire_division(l_division);
			}
		}
	}
	return l_result;
}

PyObject* py_division_dividende(PyObject* a_self, PyObject* a_args)
{
	division* l_division = NULL;
	int l_dividende;
	PyObject* l_result = NULL;
	if(PyArg_ParseTuple(a_args, "n", &l_division)) {
		l_dividende = dividende_division(l_division);
		if (a_erreur_division(l_division)) {
			PyErr_SetString(division_erreur, erreur_division(l_division));
		} else {
			l_result = Py_BuildValue("i", l_dividende);
		}
	}
	return l_result;
}

PyObject* py_division_assigne_dividende(PyObject* a_self, PyObject* a_args)
{
	division* l_division = NULL;
	int l_dividende;
	PyObject* l_result = NULL;
	if(PyArg_ParseTuple(a_args, "ni", &l_division, &l_dividende)) {
		assigne_dividende_division(l_division, l_dividende);
		if (a_erreur_division(l_division)) {
			PyErr_SetString(division_erreur, erreur_division(l_division));
		} else {
			Py_INCREF(Py_None);
			l_result = Py_None;
		}
	}
	return l_result;
}

PyObject* py_division_diviseur(PyObject* a_self, PyObject* a_args)
{
	division* l_division = NULL;
	int l_diviseur;
	PyObject* l_result = NULL;
	if(PyArg_ParseTuple(a_args, "n", &l_division)) {
		l_diviseur = diviseur_division(l_division);
		if (a_erreur_division(l_division)) {
			PyErr_SetString(division_erreur, erreur_division(l_division));
		} else {
			l_result = Py_BuildValue("i", l_diviseur);
		}
	}
	return l_result;
}

PyObject* py_division_assigne_diviseur(PyObject* a_self, PyObject* a_args)
{
	division* l_division = NULL;
	int l_diviseur;
	PyObject* l_result = NULL;
	if(PyArg_ParseTuple(a_args, "ni", &l_division, &l_diviseur)) {
		assigne_diviseur_division(l_division, l_diviseur);
		if (a_erreur_division(l_division)) {
			PyErr_SetString(division_erreur, erreur_division(l_division));
		} else {
			Py_INCREF(Py_None);
			l_result = Py_None;
		}
	}
	return l_result;
}

PyObject* py_division_division(PyObject* a_self, PyObject* a_args)
{
	division* l_division = NULL;
	float l_resultat_division;
	PyObject* l_result = NULL;
	if(PyArg_ParseTuple(a_args, "n", &l_division)) {
		l_resultat_division = division_division(l_division);
		if (a_erreur_division(l_division)) {
			PyErr_SetString(division_erreur, erreur_division(l_division));
		} else {
			l_result = Py_BuildValue("f", l_resultat_division);
		}
	}
	return l_result;
}


PyObject* py_division_detruire(PyObject* a_self, PyObject* a_args)
{
	division* l_division = NULL;
	PyObject* l_result = NULL;
	if(PyArg_ParseTuple(a_args, "n", &l_division)) {
		detruire_division(l_division);
		Py_INCREF(Py_None);
		l_result = Py_None;
	}
	return l_result;
}

PyMethodDef division_methodes[] = {
	{"creer", py_division_creer, METH_VARARGS,
	 "Création du gestionnaire de division."},
	{"dividende", py_division_dividende, METH_VARARGS,
	 "La dividende de la division."},
	{"assigne_dividende", py_division_assigne_dividende, METH_VARARGS,
	 "Assigne la dividende de la division."},
	{"diviseur", py_division_diviseur, METH_VARARGS,
	 "Le diviseur de la division."},
	{"assigne_diviseur", py_division_assigne_diviseur, METH_VARARGS,
	 "Assigne le diviseur de la division."},
	{"division", py_division_division, METH_VARARGS,
	 "Le résultat de la division."},
	{"detruire", py_division_detruire, METH_VARARGS,
	 "libération du gestionnaire de division."},
	{NULL, NULL, 0, NULL}
};

struct PyModuleDef division_module = {
	PyModuleDef_HEAD_INIT,
	"division_externe",
	"Gestionnaire de division.",
	-1,
	division_methodes
};


PyMODINIT_FUNC PyInit_division_externe(void){
	PyObject* l_module;
	l_module = PyModule_Create(&division_module);
	if (l_module) {
		division_erreur = PyErr_NewException("division_externe.erreur", NULL, NULL);
		Py_INCREF(division_erreur);
		PyModule_AddObject(l_module, "erreur", division_erreur);
	}
	return l_module;
}
