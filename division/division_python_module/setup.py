from distutils.core import setup, Extension

division_externe = Extension("division_externe",
                                include_dirs = ["../division/"],
                                libraries = ["division"],
                                library_dirs = ["../division/bin/Release/"],
                                sources = ["division_module.c"]
                                )

setup (name = "division_externe",
        version = "0.1",
        description = "Gestionnaire de division.",
        ext_modules = [division_externe]
        )
