#include <stdbool.h>

#define ERREUR_TAILLE 255

/*
    \file arbre_binaire.h
    Fichier d'entête contenant la structure de donnée "arbre binaire".
    Un arbre binaire est un graphe connexe acyclique orienté où chaque noeud 
    peut pointer vers ses enfants et qui contient des valeurs quelconques.
*/ 

/*
    Structure implémentée d'arbre binaire.
*/
typedef struct noeudArbre noeudArbre;

/*
    \brief Fonction qui créer un arbre binaire en insérant la valeur "valeur" dans la racine de l'arbre.
    \param int valeur La valeur à mettre dans la racine de l'arbre.
    \return Retourne le pointeur de la racine de l'arbre.
*/ 
noeudArbre* creer_arbre_binaire(int valeur);

/*
    \brief Fonction qui prend le contenu d'un fichier binaire et qui le charge dans un nouvel arbre.
    \param char* nom_fichier Le nom du fichier où les informations sont storées.
    \note charger_arbre_binaire fera appel à creer_arbre_binaire.
    \return Retourne le pointeur de la racine du nouvel arbre chargé.
*/
noeudArbre* charger_arbre_binaire(char *nom_fichier);

/*
    \brief Fonction qui détruit un arbre binaire et qui libère l'espace mémoire de cet arbre.
    \param noeudArbre* arbre L'arbre à détruire.
*/
void detruire_arbre_binaire(noeudArbre* arbre);

/* 
    \brief Fonction qui sauvegarde le contenu d'un arbre binaire dans un fichier binaire "nom_fichier".
    \param noeudArbre* arbre L'arbre qui sera sauvegarder dans le fichier.
    \param char* nom_fichier Le nom du fichier où stocker l'information.
*/
void sauvegarder_arbre_binaire(noeudArbre* arbre, char * nom_fichier);

/* 
    \brief Fonction qui retourne le nombre de noeuds que l'arbre "arbre" contient.
    \param noeudArbre* arbre L'arbre à parcourir.
    \return Retourne le nombre d'élément dans l'arbre.
*/
int nombre_elements_arbre_binaire(noeudArbre* arbre);

/*
    \brief Fonction qui retourne le nombre de feuille contenue dans l'arbre "arbre".
    \param noeudArbre* arbre L'arbre à parcourir.
    \return Le nombre de feuilles dans l'arbre.
*/
int nombre_feuilles_arbre_binaire(noeudArbre* arbre);

/*
    \brief Fonction qui retourne la hauteur de l'arbre "arbre".
    \param noeudArbre* arbre L'arbre à parcourir.
    \return La hauteur de l'arbre.
*/
int hauteur_arbre_binaire(noeudArbre* arbre);

/*
    \brief Fonction qui retourne l'élément à la racine de l'arbre.
    \param noeudArbre* arbre L'arbre à parcourir.
    \return L'élément à la racine de l'arbre.
*/
int element_arbre_binaire(noeudArbre* arbre);

/*
    \brief Fonction qui modifie l'élément à la racine de l'arbre "arbre" par la valeur "valeur".
    \param noeudArbre* arbre L'arbre à modifier la valeur de la racine.
    \param int valeur La valeur qui replacera celle de la racine de l'arbre.
*/
void modifier_element_arbre_binaire(noeudArbre* arbre, int valeur);

/*
    \brief Fonction qui parcourt l'arbre à la recherche de la valeur "valeur". Retourne vrai si
    la valeur est contenue dans l'arbre, sinon retourne faux.
    \param noeudArbre* arbre L'arbre à parcourir.
    \param int valeur La valeur à verifier si elle est contenue dans l'arbre.
    \return Retourne vrai si la valeur est contenue dans l'arbre, sinon retourne faux.
*/
bool contient_element_arbre_binaire(noeudArbre* arbre, int valeur);

/*
    \brief Fonction qui retourne le sous-arbre enfant gauche de l'arbre "arbre" ou NULL si
    l'arbre n'a pas d'enfant gauche.
    \param noeudArbre* arbre L'arbre à verifier.
    \return Retourne le sous-arbre gauche de l'arbre "arbre".
*/
noeudArbre* enfant_gauche_arbre_binaire(noeudArbre* arbre);

/*
    \brief Fontion qui créer un enfant gauche sur l'arbre "arbre". Si l'arbre a déjà un enfant gauche,
    la fonction ne modifie rien et retourne une erreur.
    \param noeudArbre* arbre L'arbre où rajouter le nouvel enfant.
    \param int valeur La valeur à insérer dans le nouvel enfant.
*/
void creer_enfant_gauche_arbre_binaire(noeudArbre* arbre, int valeur);

/*
    \brief Fonction qui retire l'enfant gauche de l'arbre "arbre" ainsi que tous les sous-arbres de
    l'enfant originel.
    \param noeudArbre* arbre L'arbre où retirer son enfant gauche.
*/
void retirer_enfant_gauche_arbre_binaire(noeudArbre* arbre);

/*
    \brief Fonction qui retourne le sous-arbre enfant droit de l'arbre "arbre" ou NULL si
    l'arbre n'a pas d'enfant droit.
    \param noeudArbre* arbre L'arbre à verifier.
    \return Retourne le sous-arbre droit de l'arbre "arbre".
*/
noeudArbre* enfant_droit_arbre_binaire(noeudArbre* arbre);

/*
    \brief Fontion qui créer un enfant droit sur l'arbre "arbre". Si l'arbre a déjà un enfant droit,
    la fonction ne modifie rien et retourne une erreur.
    \param noeudArbre* arbre L'arbre où rajouter le nouvel enfant.
    \param int valeur La valeur à insérer dans le nouvel enfant.
*/
void creer_enfant_droit_arbre_binaire(noeudArbre* arbre, int valeur);

/*
    \brief Fonction qui retire l'enfant droit de l'arbre "arbre" ainsi que tous les sous-arbres de
    l'enfant à retirer.
    \param noeudArbre* arbre L'arbre où retirer son enfant droit.
*/
void retirer_enfant_droit_arbre_binaire(noeudArbre* arbre);

/*
    \brief Fonction qui retourne vrai si une erreur est survenue lors du dernier appel de fonction
    de la librairie, sinon retourne faux.
    \param noeudArbre* arbre L'arbre où le boolean est gardé.
    \return Retourne vrai/faux si une erreur est survenue.
*/
bool a_erreur_arbre_binaire(noeudArbre* arbre);

/*
    \brief Fonction qui retourne un message d'erreur si "a_erreur_arbre_binaire" est vrai, 
    sinon retourne rien.
    \param noeudArbre* arbre L'arbre où le message d'erreur est gardé.
    \return Retourne le message d'erreur.
*/
char* erreur_arbre_binaire(noeudArbre* arbre);

/*
    \brief Fonction qui insert un message d'erreur dans l'arbre binaire. Met également 
    "a_erreur_arbre_binaire" à vrai.
    \param noeudArbre* arbre L'arbre où le message sera inscrit.
    \param char* erreur Le message d'erreur à mettre dans l'arbre.
*/
void inscrire_erreur_arbre_binaire(noeudArbre* arbre, const char* erreur);

/*
    \brief Fonction qui met "a_erreur_arbre_binaire" à faux.
    \param noeudArbre* arbre L'arbre où "a_erreur_arbre_binaire" sera changé à faux.
*/
void retirer_erreur_arbre_binaire(noeudArbre* arbre);