#ifndef _division_h
#define _division_h

#include <stdbool.h>

#define ERREUR_TAILLE 255

typedef struct division_struct division;

division* creer_division(int a_dividende, int a_diviseur);

void detruire_division(division* a_division);

int dividende_division(division* a_division);

void assigne_dividende_division(division* a_division, int a_dividende);

int diviseur_division(division* a_division);

void assigne_diviseur_division(division* a_division, int a_diviseur);

float division_division(division* a_division);

bool a_erreur_division(division* a_division);

char* erreur_division(division* a_division);

void inscrire_erreur_division(division* a_division, const char* a_erreur);

void retirer_erreur_division(division* a_division);

#endif /* _division_h */
