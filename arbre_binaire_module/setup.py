from distutils.core import setup, Extension

arbre_binaire = Extension("arbre_binaire",
                                include_dirs = ["../arbre_binaire/"],
                                libraries = ["arbre_binaire"],
                                library_dirs = ["../arbre_binaire/bin/Release/"],
                                sources = ["arbre_binaire_module.c"]
                                )

setup (name = "arbre_binaire",
        version = "0.1",
        description = "Gestionnaire d'arbre binaire.",
        ext_modules = [arbre_binaire]
        )
