/*

    Copyright (c) 2019 Xavier Blanchette-Noël

    L'autorisation est accordée, gracieusement, à toute personne acquérant une
    copie de cette bibliothèque et des fichiers de documentation associés
    (la "Bibliothèque"), de commercialiser la Bibliothèque sans restriction,
    notamment les droits d'utiliser, de copier, de modifier, de fusionner, de
    publier, de distribuer, de sous-licencier et / ou de vendre des copies de
    la Bibliothèque, ainsi que d'autoriser les personnes auxquelles la
    Bibliothèque est fournie à le faire, sous réserve des conditions suivantes:

    La déclaration de copyright ci-dessus et la présente autorisation doivent
    être incluses dans toutes copies ou parties substantielles de la
    Bibliothèque.

    LA BIBLIOTHÈQUE EST FOURNIE "TELLE QUELLE", SANS GARANTIE D'AUCUNE SORTE,
    EXPLICITE OU IMPLICITE, NOTAMMENT SANS GARANTIE DE QUALITÉ MARCHANDE,
    D’ADÉQUATION À UN USAGE PARTICULIER ET D'ABSENCE DE CONTREFAÇON. EN AUCUN
    CAS, LES AUTEURS OU TITULAIRES DU DROIT D'AUTEUR NE SERONT RESPONSABLES DE
    TOUT DOMMAGE, RÉCLAMATION OU AUTRE RESPONSABILITÉ, QUE CE SOIT DANS LE
    CADRE D'UN CONTRAT, D'UN DÉLIT OU AUTRE, EN PROVENANCE DE, CONSÉCUTIF À OU
    EN RELATION AVEC LA BIBLIOTHÈQUE OU SON UTILISATION, OU AVEC D'AUTRES
    ÉLÉMENTS DE LA BIBLIOTHÈQUE.

*/

/*
    \file main.c
    Fichier contenant les tests pour les arbres binaires.
*/

#include <stdio.h>
#include <stdlib.h>

#include "arbre_binaire.h"

/*
    \brief Fonction main qui teste les fonctionnalités de la librairie arbre_binaire.c
*/
int main() 
{   
    printf("Création de la racine de l'arbre.\n");
    noeudArbre* noeud = creer_arbre_binaire(2);
    printf("L'arbre contient %d element.\n", nombre_elements_arbre_binaire(noeud));
    printf("L'arbre contient %d feuille.\n", nombre_feuilles_arbre_binaire(noeud));
    printf("La hauteur de l'arbre est de %d.\n\n", hauteur_arbre_binaire(noeud));
    
    printf("Ajout de 2 enfants sur la racine de l'arbre.\n\n");
    
    creer_enfant_gauche_arbre_binaire(noeud, 15);
    creer_enfant_droit_arbre_binaire(noeud, 150);
    printf("L'arbre contient maintenant %d elements.\n", nombre_elements_arbre_binaire(noeud));
    printf("L'arbre contient %d feuilles.\n", nombre_feuilles_arbre_binaire(noeud));
    printf("La hauteur de l'arbre est de %d.\n", hauteur_arbre_binaire(noeud));

    printf("\nL'arbre est constitué de la racine ainsi que 2 enfants.\n\n");

    printf("Ajout de 2 enfants sur la branche gauche de l'enfant de la racine de l'arbre.\n");
    creer_enfant_gauche_arbre_binaire(enfant_gauche_arbre_binaire(noeud), 2000);
    creer_enfant_droit_arbre_binaire(enfant_gauche_arbre_binaire(noeud), 1000);
    printf("L'arbre contient maintenant %d elements.\n", nombre_elements_arbre_binaire(noeud));
    printf("L'arbre contient %d feuilles.\n", nombre_feuilles_arbre_binaire(noeud));
    printf("La hauteur de l'arbre est de %d.\n", hauteur_arbre_binaire(noeud));

    sauvegarder_arbre_binaire(noeud, "test.bin");
    printf("\n");
    
    printf("On retire l'enfant gauche de l'enfant gauche de la racine.\n");
    retirer_enfant_gauche_arbre_binaire(enfant_gauche_arbre_binaire(noeud));
    printf("L'arbre contient maintenant %d elements.\n", nombre_elements_arbre_binaire(noeud));
    printf("L'arbre contient %d feuilles.\n", nombre_feuilles_arbre_binaire(noeud));
    printf("La hauteur de l'arbre est de %d.\n\n", hauteur_arbre_binaire(noeud));
    
    int element_contenu = 50;
    printf("On cherche si l'élément 50 est dans l'arbre.\n");
    if (contient_element_arbre_binaire(noeud, element_contenu)) {
        printf("L'élément %d est contenu dans l'arbre donné.\n", element_contenu);
    } else {
        printf("L'élément %d n'est pas contenu dans l'arbre donné.\n", element_contenu);
    }

    element_contenu = 150;
    printf("\nOn cherche si l'élément 150 est dans l'arbre.\n");
    if (contient_element_arbre_binaire(noeud, element_contenu)) {
        printf("L'élément %d est contenu dans l'arbre donné.\n", element_contenu);
    } else {
        printf("L'élément %d n'est pas contenu dans l'arbre donné.\n", element_contenu);
    }

    modifier_element_arbre_binaire(noeud, 1500);
    printf("\nLe premier élément de l'arbre était 2, mais après modification est devenu : %d.\n", element_arbre_binaire(noeud));


    detruire_arbre_binaire(noeud);
    printf("\nL'arbre #1 a été supprimé avec succès.\n");

    noeudArbre* noeud2 = charger_arbre_binaire("test.bin");

    printf("\n");

    printf("Le premier élément de l'arbre est : %d.\n", element_arbre_binaire(noeud2));
    printf("L'arbre contient %d element.\n", nombre_elements_arbre_binaire(noeud2));
    printf("L'arbre contient %d feuille.\n", nombre_feuilles_arbre_binaire(noeud2));
    printf("La hauteur de l'arbre est de %d.\n\n", hauteur_arbre_binaire(noeud2));

    detruire_arbre_binaire(noeud2);

    return 0;
}